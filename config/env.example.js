'use strict'

module.exports = {
  NODE_ENV: '"develop"',
  ROOT_API: '"http://localhost:8000/api"',
  SKILLS: true,
  MENU: {
    WORKAPPLICATIONS: true,
    ARK: true,
    BROKER: true,
    SHOWLOGS: true,
  },
}