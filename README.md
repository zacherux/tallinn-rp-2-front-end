# tallinn-rp-web-interface-2

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Css

Documentation of [css](https://demos.creative-tim.com/material-kit/docs/2.1/getting-started/introduction.html)
