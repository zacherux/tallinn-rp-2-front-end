import Vue from 'vue'
import VueRouter from 'vue-router'

import { store } from './store/store'
import mainHome from './components/main/home.vue'
import loginHeader from './components/logedIn/parts/headerNavBar.vue'
import userHome from './components/logedIn/userDetails/userProfile.vue'
import wlStatusVue from './components/logedIn/userDetails/whitelist/status.vue'
import stasboardBaseVue from './components/logedIn/parts/dashboardBase.vue'
import userCars from './components/logedIn/userDetails/userCars.vue'
import userCriminalRecords from './components/logedIn/userDetails/userCriminalRecords.vue'
import userMedicalRecords from './components/logedIn/userDetails/userMedicalRecords.vue'
import policeDashboard from './components/logedIn/police/dashboard.vue'
import ambulanceDashboard from './components/logedIn/ambulance/dashboard.vue'
import ambulanceWorkersList from './components/logedIn/ambulance/workersList.vue'
import arkDashboard from './components/logedIn/ark/dashboard.vue'
import mechanicDashboard from './components/logedIn/mechanic/dashboard.vue'
import mechanicForm from './components/logedIn/mechanic/reportForm.vue'
import mechanicWorkersList from './components/logedIn/mechanic/workersList.vue'
import mechanicPriceList from './components/logedIn/mechanic/priceList.vue'
import brokerDashboard from './components/logedIn/broker/dashboard.vue'
import brokerAvailableProperties from './components/logedIn/broker/availableProperties.vue'
import brokerProperties from './components/logedIn/broker/Properties.vue'
import brokerProperty from './components/logedIn/broker/property.vue'
import userProfile from './components/logedIn/user/profile.vue'
import searchedUserCars from './components/logedIn/user/userCars.vue'
import searchedUserCriminalRecords from './components/logedIn/user/userCriminalRecords'
import searchedUserMedicallRecords from './components/logedIn/user/userMedicalRecords'
import adminDashboard from './components/logedIn/admin/dashboard.vue'
import adminWhitelistApplicationList from './components/logedIn/admin/whitelistAppList.vue'
import adminChangeRules from './components/logedIn/admin/rules.vue'
import adminChangeInfo from './components/logedIn/admin/infoArea.vue'
import adminPriorityList from './components/logedIn/admin/priorityList.vue'
import logedInRules from './components/logedIn/serverRules.vue'
import logedIninfo from './components/logedIn/infoArea.vue'
import mainLogs from './components/logedIn/logs/main.vue'
import donation from './components/logedIn/donation.vue'
import adminDonations from './components/logedIn/admin/donations.vue'
import applicationsHome from './components/logedIn/applications/home.vue'
import workApplications from './components/logedIn/applications/workApplicationsForCompany.vue'
import gangAppList from './components/logedIn/admin/gangAppList.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '*',
        component: mainHome
    },
    {
        path: '/',
        name: 'index',
        components: {
            content: mainHome
        }
    },
    {
        path: '/home',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'userHome',
                component: userHome
            },
            {
                path: 'cars',
                name: 'userCars',
                component: userCars
            },
            {
                path: 'criminalRecords',
                name: 'userCriminalRecords',
                component: userCriminalRecords
            },
            {
                path: 'medicalRecords',
                name: 'userMedicalRecords',
                component: userMedicalRecords
            }
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/police',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'policeDashboard',
                component: policeDashboard
            }
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/ambulance',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'ambulanceDashboard',
                component: ambulanceDashboard
            },
            {
                path: 'workersList',
                name: 'emsWorkersList',
                component: ambulanceWorkersList
            },
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/ark',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'arkDashboard',
                component: arkDashboard
            }
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/mechanic',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'mechanicDashboard',
                component: mechanicDashboard
            },
            {
                path: 'reportForm',
                name: 'mechanicReportForm',
                component: mechanicForm
            },
            {
                path: 'workers',
                name: 'mechanicWorkersList',
                component: mechanicWorkersList,
            },
            {
                path: 'priceList',
                name: 'mechanicPriceList',
                component: mechanicPriceList,
            }
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/broker',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'brokerDashboard',
                component: brokerDashboard
            },
            {
                path: 'availableProperties',
                name: 'availableProperties',
                component: brokerAvailableProperties
            },
            {
                path: 'properties',
                name: 'properties',
                component: brokerProperties
            },
            {
                path: 'property/:id',
                name: 'property',
                component: brokerProperty
            }
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/user',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: ':hex',
                name: 'userProfile',
                component: userProfile
            },
            {
                path: ':hex/cars/:plate?',
                name: 'searchedUserCars',
                component: searchedUserCars
            },
            {
                path: ':hex/criminalRecords',
                name: 'searchedUserCriminalRecords',
                component: searchedUserCriminalRecords
            },
            {
                path: ':hex/medicalRecords',
                name: 'searchedUserMedicalRecords',
                component: searchedUserMedicallRecords
            },
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/whitelistApplication',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: 'status',
                name: 'wlStatus',
                component: wlStatusVue
            },
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/admin',
        components: {
            header: loginHeader,
            content: stasboardBaseVue,
        },
        children: [
            {
                path: '',
                name: 'adminDashboard',
                component: adminDashboard
            },
            {
                path: 'whitelistApplications',
                name: 'adminWhitelistList',
                component: adminWhitelistApplicationList
            },
            {
                path: 'rules',
                name: 'changeRules',
                component: adminChangeRules
            },
            {
                path: 'info',
                name: 'changeInfo',
                component: adminChangeInfo
            },
            {
                path: 'priorityList',
                name: 'adminPrioList',
                component: adminPriorityList
            },
            {
                path: 'donations',
                name: 'adminDonations',
                component: adminDonations
            },
            {
                path: 'gangApplications',
                name: 'adminGangApplications',
                component: gangAppList
            },
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken && (store.getters.getWebUserData.roleName == 'admin' || store.getters.getWebUserData.roleName == 'mod' || store.getters.getLogedInUserRoleName == 'admin' || store.getters.getLogedInUserRoleName == 'mod')) {
                next()
            } else {
                next({ name: 'userHome' })
            }
        }
    },
    {
        path: '/server',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: 'rules',
                name: 'serverRules',
                component: logedInRules
            },
            {
                path: 'info',
                name: 'serverInfo',
                component: logedIninfo
            },
        ]
    },
    {
        path: '/applications',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'myApplications',
                component: applicationsHome
            },
            {
                path: ':work/:type',
                name: 'workApplications',
                component: workApplications
            },
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/logs',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'mainLogs',
                component: mainLogs
            },
        ],
        beforeEnter(to, from, next) {
            //if (store.state.idToken) {
            if (store.state.idToken && (store.getters.getWebUserData.roleName == 'admin' || store.getters.getWebUserData.roleName == 'mod' || store.getters.getLogedInUserRoleName == 'admin' || store.getters.getLogedInUserRoleName == 'mod')) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
    {
        path: '/donation',
        components: {
            header: loginHeader,
            content: stasboardBaseVue
        },
        children: [
            {
                path: '',
                name: 'donation',
                component: donation
            },
        ],
        beforeEnter(to, from, next) {
            if (store.state.idToken) {
                next()
            } else {
                next({ name: 'index' })
            }
        }
    },
];

export default new VueRouter({ routes })