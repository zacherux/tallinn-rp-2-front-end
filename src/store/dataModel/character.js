class Cratacter {

    characterData = {
        identifier: null,
        fullName: null,
        dateOfBirth: null,
        sex: null,
        job: null,
        height: null,
        phoneNumber: null,
        jobGrade: null,
        money: null,
        bank: null,
        blackMoney: null,
        billsSum: null,
        jail: null,
        pet: null,
        jobLabel: null,
        jobGradeLabel: null,
        salary: null
    }

    licenses = {
        aCategory: false,
        bCategory: false,
        boat: false,
        cCategory: false,
        dCategory: false,
        fishing: false,
        hunting: false,
        theory: false,
        weapon: false,
    }
    bills = []
    companyMoney = 0
    cars = []
    properties = []
    houses = []
    criminalRecords = {
        counts: {
            total: 0,
            criminalCases: 0,
            activeCriminalCases: 0
        },
        records: []
    }
    medicalRecords = {
        counts: {
            total: 0,
            week: 0
        },
        records: []
    }
}