import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from './../routes'
import moment from 'moment'
import mainContentSlotComponent from './modules/mainContentSlotComponent'
import logedInUserDataComponent from './modules/logedInUserDataComponent'
import logedInUserCharacters from './modules/logedInUserCharaters'
import policeDashboardComponent from './modules/policeDashboardComponent'
import ambulanceDashboardComponent from './modules/ambulanceDashboardComponent'
import arkDashboardComponent from './modules/arkDashboardComponent'
import mechanicDashboardComponent from './modules/mechanicDashboardComponent'
import mechanicComponent from './modules/mechanicComponent'
import searchComponent from './modules/searchComponent'
import brokerDashboardComponent from './modules/brokerComponent'
import searchedUserData from './modules/searchedUserData'
import rules from './modules/rules'
import info from './modules/info'
import donations from './modules/donations'
import gangApplications from './modules/gangApplications'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        idToken: null,
        logedInUserId: null,
        logedInUserRoleId: null,
        logedInUserRoleName: null,
        loginError: false,
        signUpSuccess: false,
        whitelistErrors: [],
        brokerWorkNameInDb: "realestateagent",
        godRoles: ['admin', 'mod'],
        searchAllowedJobs: ['police', 'offpolice', 'ambulance', 'offambulance', 'realestateagent', 'offdeluxe', 'deluxe', 'abipolitsei'],
        carActionsAllowedJobs: ['police', 'offpolice'],
        loginLoading: false,
        signupLoading: false,
    },
    getters: {
        isAuthenticated(state) {
            return state.idToken !== null
        },
        getLoginError(state) {
            return state.loginError
        },
        getWhitelistErros(state) {
            return state.whitelistErrors
        },
        getSignUpSuccess(state) {
            return state.signUpSuccess
        },
        getToken(state) {
            return state.idToken
        },
        getBrokerworkName(state) {
            return state.brokerWorkNameInDb
        },
        getGodRoles(state) {
            return state.godRoles
        },
        getSearchAllowedJobs(state) {
            return state.searchAllowedJobs
        },
        getCarActionsAllowedJobs(state) {
            return state.carActionsAllowedJobs
        },
        getLoginLoading(state) {
            return state.loginLoading
        },
        getSignupLoading(state) {
            return state.signupLoading
        },
        getLogedInUserRoleId(state) {
            return state.logedInUserRoleId
        },
        getLogedInUserRoleName(state) {
            return state.logedInUserRoleName
        },
        getLogedInUserWebId(state) {
            return state.logedInUserId
        }
    },
    mutations: {
        authUser(state, userData) {
            state.idToken = userData.token
            state.logedInUserId = userData.userId
            state.logedInUserRoleId = userData.roleId
            state.logedInUserRoleName = userData.roleName
        },
        clearAuth(state) {
            state.idToken = null,
                state.logedInUserId = null
            state.logedInUserRoleName = null
            state.logedInUserRoleId = null
        },
        setLoginError(state, status) {
            state.loginError = status
        },
        setWhitelistErrors(state, errors) {
            state.whitelistErrors = errors
        },
        setSignUpAsSuccesful(state) {
            state.signUpSuccess = true
        },
        setLoginLoading(state, status) {
            state.loginLoading = status
        },
        setSignupLoading(state, status) {
            state.signupLoading = status
        },
        setLogedInUserRoleId(state, id) {
            state.logedInUserRoleId = id
        },
        setLogedInUserRoleName(state, role) {
            state.logedInUserRoleName = role
        }
    },
    actions: {
        signUp({ commit, dispatch }, authData) {
            commit('setSignupLoading', true)
            axios.post(
                '/registerWhitelist',
                {
                    "name": authData.steamName,
                    "email": authData.email,
                    "password": authData.password,
                    "c_password": authData.passwordAgain,
                    "characterName": authData.characterName,
                    "steamName": authData.steamName,
                    "steamHex": authData.steamHex,
                    "questions": authData.questions
                }
            ).then(response => {
                commit('setSignUpAsSuccesful')
                commit(
                    'authUser',
                    {
                        token: response.data.token,
                        userId: response.data.userId,
                        roleId: response.data.roleId,
                        roleName: response.data.roleName
                    }
                )
                let expirationDate = new Date(response.data.expiresAt)
                let now = new Date()
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('expirationDate', expirationDate)
                localStorage.setItem('userId', response.data.userId)
                dispatch('setLogoutTimer', expirationDate.getTime() - now.getTime())
                commit('setSignupLoading', false)
                router.push({ name: 'wlStatus' })

            }).catch(error => {
                if (typeof error.response != "undefined" && typeof error.response.data != "undefined") {
                    let messages = error.response.data.messages
                    console.log(messages)
                    commit('setWhitelistErrors', Object.keys(messages))
                    commit('setSignupLoading', false)
                } else {
                    console.log(error)
                    commit('setSignupLoading', false)
                }
            })
        },
        login({ commit, dispatch }, authData) {
            commit('setLoginLoading', true)
            axios.post(
                '/login',
                {
                    email: authData.email,
                    password: authData.password
                }
            ).then(response => {
                commit(
                    'authUser',
                    {
                        token: response.data.token,
                        userId: response.data.userId,
                        roleId: response.data.roleId,
                        roleName: response.data.roleName
                    }
                )
                let expirationDate = new Date(response.data.expiresAt)
                let now = new Date()
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('expirationDate', expirationDate)
                localStorage.setItem('userId', response.data.userId)
                localStorage.setItem('roleId', response.data.roleId)
                localStorage.setItem('roleName', response.data.roleName)
                dispatch('setLogoutTimer', expirationDate.getTime() - now.getTime())
                commit('setLoginError', false)
                commit('setLoginLoading', false)
                router.push({ name: 'userHome' })
            }).catch(error => {
                commit('setLoginError', true)
                commit('setLoginLoading', false)
            })
        },
        logout({ commit }) {
            commit('clearAuth')
            localStorage.removeItem('token')
            localStorage.removeItem('expirationDate')
            localStorage.removeItem('userId')
            localStorage.removeItem('roleName')
            localStorage.removeItem('roleId')

            router.replace({ name: 'index' })
        },
        setLogoutTimer({ commit }, timer) {
            setTimeout(() => {
                commit('clearAuth')
                router.replace({ name: 'index' })
            }, timer)
        },
        tryAutoLogin({ commit, dispatch }) {
            let token = localStorage.getItem('token')
            if (!token) {
                return
            }

            let expirationDate = localStorage.getItem('expirationDate')
            let now = new Date()
            if (now >= expirationDate) {
                return
            }

            let expirationDateAsNewData = new Date(expirationDate)
            commit('authUser', { token: token, userId: localStorage.getItem('userId'), roleId: localStorage.getItem('roleId'), roleName: localStorage.getItem('roleName') })
            dispatch('setLogoutTimer', expirationDateAsNewData.getTime() - now.getTime())
        }
    },
    modules: {
        mainContentSlotComponent,
        logedInUserDataComponent,
        logedInUserCharacters,
        policeDashboardComponent,
        ambulanceDashboardComponent,
        arkDashboardComponent,
        mechanicDashboardComponent,
        mechanicComponent,
        searchComponent,
        brokerDashboardComponent,
        searchedUserData,
        rules,
        info,
        donations,
        gangApplications
    }
})