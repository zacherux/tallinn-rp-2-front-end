import axios from 'axios'

const state = {
    counts: {
        total: 0,
        waiting: 0
    },
    gangApplications: [],
    gangApplicationsLoading: false
}
const getters = {
    getGangCounts(state) {
        return state.counts
    },
    getGangApplications(state) {
        return state.gangApplications
    },
    getGangApplicationsLoading(state) {
        return state.gangApplicationsLoading
    }
}
const mutations = {
    setCounts(state, counts) {
        state.counts.total = counts.total,
            state.counts.waiting = counts.waiting
    },
    setGangApplications(state, applications) {
        state.gangApplications = applications
    },
    setGangApplicationsLoading(state, status) {
        state.gangApplicationsLoading = status
    }
}
const actions = {
    counts({ commit }, token) {
        axios.get('gangApplication/getCounts', {
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(response => {
            commit('setCounts', response.data)
        }).catch(error => {
            console.log(error)
        })
    },
    gangApplications({ commit }, token) {
        commit('setGangApplicationsLoading', true)
        axios.get('gangApplication/get',
            { headers: { 'Authorization': 'Bearer ' + token } }
        ).then(response => {

            commit('setGangApplications', response.data.gangApplications)
            commit('setGangApplicationsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('setGangApplicationsLoading', false)
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}