import axios from 'axios'

const state = {
    info: [],
    loading: {
        info: false
    }
}
const getters = {
    getInfo(state) {
        return state.info
    },
    isInfoLoading(state) {
        return state.loading.info
    },
}
const mutations = {
    setInfo(state, info) {
        state.info = info
    },
    toggleInfoLoading(state, isLoading) {
        state.loading.info = isLoading
    },
}
const actions = {
    getInfo({ commit }, token) {
        commit('toggleInfoLoading', true)
        
        axios.get(
            'getAllInfo',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setInfo', response.data.info)
            
            commit('toggleInfoLoading', false)

        }).catch(error => {
            console.log(error)
            commit('toggleInfoLoading', false)
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions
}