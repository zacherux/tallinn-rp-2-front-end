import axios from 'axios'

const state = {
    mechanicWorkers: [],
    loading: {
        mechanicWorker: false
    }
}
const getters = { 
    getMechanicWorkers(state) {
        return state.mechanicWorkers
    },
    isMechanicWorkersLoading(state) {
        return state.loading.mechanicWorker
    },
}
const mutations = {
    setMechanicWorkers(state, workers) {
        state.mechanicWorkers = workers
    },
    toggleMechanicWorkersLoading(state, isLoading) {
        state.loading.mechanicWorker = isLoading
    }
}
const actions = {
    mechanicWorkersAction({ commit }, token) {
        commit('toggleMechanicWorkersLoading', true)
        axios.get(
            'mechanic/getMechanicWorkers',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setMechanicWorkers', response.data)
            commit('toggleMechanicWorkersLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleMechanicWorkersLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}