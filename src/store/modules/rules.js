import axios from 'axios'

const state = {
    rules: [],
    loading: {
        rules: false
    }
}
const getters = {
    getRules(state) {
        return state.rules
    },
    isRulesLoading(state) {
        return state.loading.rules
    },
}
const mutations = {
    setRules(state, rules) {
        state.rules = rules
    },
    toggleRulesLoading(state, isLoading) {
        state.loading.rules = isLoading
    },
}
const actions = {
    getRules({ commit }, token) {
        commit('toggleRulesLoading', true)
        
        axios.get(
            'getRules',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setRules', response.data.rules)
            
            commit('toggleRulesLoading', false)

        }).catch(error => {
            console.log(error)
            commit('toggleRulesLoading', false)
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions
}