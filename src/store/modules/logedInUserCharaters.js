import axios from 'axios'
import config from './../../../config/env'

const state = {
    activeCharacterIdentifier: null,
    charactersIdentifiers: [],
    characters: {},
    navBarInfo: [],
    charactersLoading: false,
}
const getters = {
    getCharacters(state) {
        return state.characters
    },
    getCharactersInfoForNavBar(state) {
        return state.navBarInfo
    },
    getCharacter: (state) => (id) => {
        return state.characters[id]
    },
    getCharactesIdentifiers() {
        return state.charactersIdentifiers
    },
    isCharactesLoading(state) {
        return state.charactersLoading
    },
    getActiveCharacterIdendtifier(state) {
        return state.activeCharacterIdentifier
    }
}
const mutations = {
    setCharacters(state, data) {
        state.characters = {}
        state.navBarInfo = []
        data.forEach(character => {
            let identifier = character.identifier
            let accounts = JSON.parse(character.accounts)
            let billSum = 0;
            let softSkills = {
                stamina: 0,
                shooting: 0,
                strength: 0
            }

            character.bills.forEach(bill => {
                billSum += bill.amount
            })

            character.soft_skills.forEach(skill => {
                switch (skill.type) {
                    case 'MP0_SHOOTING_ABILITY':
                        softSkills.shooting = skill.amount
                        break
                    case 'MP0_STAMINA':
                        softSkills.stamina = skill.amount
                        break
                    case 'MP0_STRENGTH':
                        softSkills.strength = skill.amount
                        break
                }
            })

            state.characters[identifier] = {
                identifier: identifier,
                fullName: character.firstname + ' ' + character.lastname,
                dateOfBirth: character.dateofbirth,
                sex: (character.sex && character.sex == 'm') ? 'Mees' : 'Naine',
                job: character.job,
                jobGrade: character.job_grade,
                height: character.height,
                phoneNumber: character.phone_number,
                money: accounts.money,
                bank: accounts.bank,
                blackMoney: accounts.black_money,
                billsSum: billSum,
                jobLabel: character.jobLabel,
                jobGradeLabel: character.jobGradeLabel,
                salary: character.salary,
                licenses: {
                    aCategory: character.modifiedLicenses.aCategory,
                    bCategory: character.modifiedLicenses.bCategory,
                    boat: character.modifiedLicenses.boat,
                    cCategory: character.modifiedLicenses.cCategory,
                    dCategory: character.modifiedLicenses.dCategory,
                    fishing: character.modifiedLicenses.fishing,
                    hunting: character.modifiedLicenses.hunting,
                    theory: character.modifiedLicenses.theory,
                    weapon: character.modifiedLicenses.weapon,
                },
                bills: character.bills,
                cars: character.cars,
                carsCounts: {
                    total: character.cars.counts.total,
                    outsideGarage: character.cars.counts.outsideGarage
                },
                properties: (character.properties) ? character.properties : [],
                houses: (character.house) ? character.house : [],
                criminalRecords: {
                    counts: {
                        total: character.criminalRecordsCount.total,
                        criminalCases: character.criminalRecordsCount.criminalCases,
                        activeCriminalCases: character.criminalRecordsCount.activeCriminalCases
                    },
                    records: character.criminal_records
                },
                medicalRecords: {
                    counts: {
                        total: character.medicalCasesCount.total,
                        week: character.medicalCasesCount.week
                    },
                    records: character.medical_history
                },
                jobApplications: character.job_application,
                gangApplications: character.gang_application,
                softSkills: softSkills
            }
            state.navBarInfo.push({
                identifier: character.identifier,
                shortenName: character.firstname.charAt(0) + '. ' + character.lastname
            })

            state.charactersIdentifiers.push(identifier)
        })
    },
    setCharactersLoading(state, status) {
        state.charactersLoading = status
    },
    setActiveCharacterIdentifier(state, identifier) {
        state.activeCharacterIdentifier = identifier
    }
}
const actions = {
    getCharacters({ commit }, token) {
        commit('setCharactersLoading', true)
        axios.get(
            'auth/user/getCharacters',
            {
                params: { isSkills: config.SKILLS },
                headers: { 'Authorization': 'Bearer ' + token }
            }
        ).then(response => {
            commit('setCharacters', response.data)
            commit('setCharactersLoading', false)
        }).catch(error => {
            console.log(error)
            commit('setCharactersLoading', false)
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}