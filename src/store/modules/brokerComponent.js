import axios from 'axios'

const state = {
    brokerAvailableProperiesForDashboard: [],
    brokerAvailableProperies: [],
    brokerAllProperiesForDashboard: [],
    brokerallProperties: [],
    loading: {
        brokerAvailableProperiesForDashboard: false,
        brokerAvailableProperies: false,
        brokerAllProperiesForDashboard: false,
        brokerallProperties: false
    }
}
const getters = { 
    getBrokerAvailableProperiesForDashboard(state) {
        return state.brokerAvailableProperiesForDashboard
    },
    isBrokerAvailableProperiesForDashboardLoading(state) {
        return state.loading.brokerAvailableProperiesForDashboard
    },
    getBrokerAvailableProperies(state) {
        return state.brokerAvailableProperies
    },
    isBrokerAvailableProperiesLoading(state) {
        return state.loading.brokerAvailableProperies
    },
    getBrokerAllProperiesForDashboard(state) {
        return state.brokerAllProperiesForDashboard
    },
    isBrokerAllProperiesForDashboardLoading(state) {
        return state.loading.brokerAllProperiesForDashboard
    },
    getBrokerAllProperties(state) {
        return state.brokerallProperties
    },
    isBrokerAllPropertiesLoading(state) {
        return state.loading.brokerallProperties
    },
}
const mutations = {
    setBrokerAvailableProperiesForDashboard(state, properties) {
        state.brokerAvailableProperiesForDashboard = properties
    },
    toggleBrokerAvailableProperiesForDashboardLoading(state, isLoading) {
        state.loading.brokerAvailableProperiesForDashboard = isLoading
    },
    setBrokerAvailableProperies(state, properties) {
        state.brokerAvailableProperies = properties
    },
    toggleBrokerAvailableProperiesLoading(state, isLoading) {
        state.loading.brokerAvailableProperies = isLoading
    },
    setBrokerAllProperiesForDashboard(state, properties) {
        state.brokerAllProperiesForDashboard = properties
    },
    toggleBrokerAllProperiesForDashboardLoading(state, isLoading) {
        state.loading.brokerAllProperiesForDashboard = isLoading
    },
    setBrokerAllProperies(state, properties) {
        state.brokerallProperties = properties
    },
    toggleAllProperiesLoading(state, isLoading) {
        state.loading.brokerallProperties = isLoading
    }
}
const actions = {
    availablepropertyForDashboardAction({ commit }, token) {
        commit('toggleBrokerAvailableProperiesForDashboardLoading', true)
        axios.get(
            'broker/getAvailablePropertiesForDashboard',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setBrokerAvailableProperiesForDashboard', response.data.properties)
            commit('toggleBrokerAvailableProperiesForDashboardLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleBrokerAvailableProperiesForDashboardLoading', false)
        })
    },
    availablepropertyAction({ commit }, token) {
        commit('toggleBrokerAvailableProperiesLoading', true)
        axios.get(
            'broker/getAvailableProperties',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setBrokerAvailableProperies', response.data.properties)
            commit('toggleBrokerAvailableProperiesLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleBrokerAvailableProperiesLoading', false)
        })
    },
    allPropertyForDashboardAction({ commit }, token) {
        commit('toggleBrokerAllProperiesForDashboardLoading', true)
        axios.get(
            'broker/getAllPropertiesForDashboard',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setBrokerAllProperiesForDashboard', response.data.properties)
            commit('toggleBrokerAllProperiesForDashboardLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleBrokerAllProperiesForDashboardLoading', false)
        })
    },
    allPropertyAction({ commit }, token) {
        commit('toggleAllProperiesLoading', true)
        axios.get(
            'broker/getAllProperties',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setBrokerAllProperies', response.data.properties)
            commit('toggleAllProperiesLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleAllProperiesLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}