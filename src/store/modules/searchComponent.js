import axios from 'axios'

const state = {
    searchWord: null,
    searchResults: [],
    loading: {
        searching: false
    }
}
const getters = { 
    getSearchWord(state) {
        return state.searchWord
    },
    getSearchResults(state) {
        return state.searchResults
    },
    isSearchingLoading(state) {
        return state.loading.searching
    },
}
const mutations = {
    setSearchWord(state, workers) {
        state.searchWord = workers
    },
    setSearchResults(state, results) {
        state.searchResults = results
    },
    toggleSearchLoading(state, isLoading) {
        state.loading.searching = isLoading
    }
}
const actions = {
    searchAction({ commit }, token) {
        commit('toggleSearchLoading', true)
        axios.post(
            'search/getResults',
            {search: state.searchWord},
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setSearchResults', response.data.results)
            commit('toggleSearchLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleSearchLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}