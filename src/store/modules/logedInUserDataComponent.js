import axios from 'axios'

const state = {
    webUserData: {
        email: null,
        name: null,
    },
    punsihments: [],
    isBanned: false,
    priority: null,
    loading: {
        userData: false,
    },
    whitelistApplictionOnReview: false,
    darkMode: false
}

const getters = {
    getWebUserData(state) {
        return state.webUserData
    },
    getPunishemtns(state) {
        return state.punsihments
    },
    isBanned(state) {
        return state.isBanned
    },
    getPriorityData(state) {
        return state.priority
    },
    isWhitelistApplicationOnReview(state) {
        return state.whitelistApplictionOnReview
    },
    isUserDataLoading(state) {
        return state.loading.userData
    },
    getDarkMode(state) {
        return state.darkMode
    }
}

const mutations = {
    setWebUserData(state, webUserData) {
        state.webUserData.email = webUserData.email
        state.webUserData.name = webUserData.name
    },
    setPunishments(state, data) {
        state.punsihments = data
    },
    setIsBanned(state, data) {
        state.isBanned = data
    },
    setPrioritydata(state, data) {
        state.priority = data
    },
    setWhitelistStatusOnReview(state, status) {
        state.whitelistApplictionOnReview = status
    },
    toggleUserDataLoading(state, status) {
        state.loading.userData = status
    },
    setDarkMode(state, status) {
        localStorage.setItem('darkMode', status)
        state.darkMode = status
    }
}

const actions = {
    getUserData({ commit }, token) {
        commit('toggleUserDataLoading', true)
        axios.get(
            'auth/user/getUserData',
            { headers: { 'Authorization': 'Bearer ' + token } }
        ).then(response => {
            commit('setWebUserData', {
                email: response.data.user.email,
                name: response.data.user.name,
            })
            commit('setPunishments', response.data.user.ban_reasons)
            commit('setIsBanned', response.data.isBanned)
            commit('setPrioritydata', response.data.user.priority)

            commit('toggleUserDataLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleUserDataLoading', false)
        })
    },
    getDarkModeFromStorage({ commit }) {
        let darkMode = (localStorage.getItem('darkMode') === 'true');
        if (!darkMode) {
            return
        }

        commit('setDarkMode', darkMode)
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}