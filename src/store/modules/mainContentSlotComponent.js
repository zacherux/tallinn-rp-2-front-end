const state = {
    mainContentSlotComponent: 'appLogin'
}

const getters= {
    getMainContentSlotComponent: state => {
        return state.mainContentSlotComponent
    }
}

const mutations = {
    setMainContentSlotComponent: (state, componentName) => {
        state.mainContentSlotComponent = componentName
    }
}

const actions = {
    setMainContentSlotComponent: ({ commit }, componentName) => {
        commit('setMainContentSlotComponent', componentName)
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}