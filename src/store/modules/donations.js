import axios from 'axios'

const state = {
    donations: {
        pending: null,
        waiting: null,
        confirmed: null
    },
    fetchingData: {
        pending: false,
        waiting: false,
        confirmed: false
    },
    loading: false,
    sum: 0
}
const getters = {
    getDonations(state) {
        return state.donations
    },
    getPendingDonations(state) {
        return state.donations.pending
    },
    getWaitingGiftsDonations(state) {
        return state.donations.waiting
    },
    getThisMonthConfirmedDonations(state) {
        return state.donations.confirmed
    },
    isFetchingData(state) {
        return state.fetchingData
    },
    isFetchingPendingData(state) {
        return state.fetchingData.pending
    },
    isFetchingWaitingGiftData(state) {
        return state.fetchingData.waiting
    },
    isFetchingConfimedData(state) {
        return state.fetchingData.confirmed
    },
    isDonationsLoading(state) {
        return state.loading
    },
    getSum(state) {
        return state.sum
    },
    getDonationPercentage(state) {
        return ( ( state.sum.sum / state.sum.goal ) * 100 ).toFixed(0)
    }
}
const mutations = {
    setPendingDonations(state, pending) {
        state.donations.pending = pending
    },
    setWaitingGiftDonations(state, waiting) {
        state.donations.waiting = waiting
    },
    setConfirmedDonations(state, confirmed) {
        state.donations.confirmed = confirmed
    },
    setSum(state, sum) {
        state.sum = sum
    },
    toggleFetchingPendingDonationStatus(state, isLoading) {
        state.fetchingData.pending = isLoading
    },
    toggleFetchingWaitingGiftStatus(state, isLoading) {
        state.fetchingData.waiting = isLoading
    },
    toggleFetchingConfirmedDonationsStatus(state, isLoading) {
        state.fetchingData.confirmed = isLoading
    },
    toggleDonationIsLoading(state, isLoading) {
        state.loading = isLoading
    }
}
const actions = {
   fetchPendingDonations({ commit }, token) {
       commit('toggleFetchingPendingDonationStatus', true)
    axios
    .get("admin/donation/getPendingDonations", {
      headers: { Authorization: "Bearer " + token },
    })
    .then((response) => {
      commit('setPendingDonations', response.data.pending)
      commit('toggleFetchingPendingDonationStatus', false)
    })
    .catch((error) => {
      console.log(error)
      commit('toggleFetchingPendingDonationStatus', false)
    });
   },
   deleteDonation({commit, state, dispatch}, data) {
    commit('toggleDonationIsLoading', true)
    axios
      .delete("admin/donation/deleteDonation", {
        data: { id: data.id },
        headers: { Authorization: "Bearer " + data.token },
      })
      .then(() => {
        state.donations[data.type].splice(data.index, 1);
        dispatch('getSum', data.token)
        commit('toggleDonationIsLoading', false)
      })
      .catch((error) => {
        console.log(error)
        commit('toggleDonationIsLoading', false)
      });
    },
    getWaitingGiftDonations({ commit, }, token) {
        commit('toggleFetchingWaitingGiftStatus', true)
        axios
          .get("admin/donation/getWaitingGift", {
            headers: { Authorization: "Bearer " + token },
          })
          .then((response) => {
            commit('setWaitingGiftDonations', response.data.waiting)
            commit('toggleFetchingWaitingGiftStatus', false)
          })
          .catch((error) => {
            console.log(error)
            commit('toggleFetchingWaitingGiftStatus', false)
          });
    },
    removeDataFromDonationArray({state},data) {
        state.donations[data.type].splice(data.index, 1);
    },
    geConfermedDonations({commit}, token) {
        commit('toggleFetchingConfirmedDonationsStatus', true)
        axios
          .get("admin/donation/getConfirmedDonations", {
            headers: { Authorization: "Bearer " + token },
          })
          .then((response) => {
            commit('setConfirmedDonations', response.data.confirmed)
            
            commit('toggleFetchingConfirmedDonationsStatus', false)
          })
          .catch((error) => {
            console.log(error)
            commit('toggleFetchingConfirmedDonationsStatus', false)
          });
    },
    getSum({commit}, token) {
        axios
          .get("admin/donation/getDonationsSum", {
            headers: { Authorization: "Bearer " + token },
          })
          .then((response) => {
            commit('setSum', response.data)
          })
          .catch((error) => {
            console.log(error)
          });
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}