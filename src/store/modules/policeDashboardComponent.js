import axios from 'axios'

const state = {
    latestCriminalRecords: [],
    topCriminalsOfWeek: {},
    topDeptors: {},
    topPoliceOfficers: {},
    jobApplciationCounts: {
        total: 0,
        unchecked: 0,
        waiting: 0
    },
    loading: {
        latestCriminalRecords: false,
        topCriminalsOfWeek: false,
        topDeptors: false,
        topPoliceOfficers: false,
        jobApplciationCounts: false
    }
}
const getters = {
    getlatestCriminalRecords(state) {
        return state.latestCriminalRecords
    },
    getTopCriminalOfWeek(state) {
        return state.topCriminalsOfWeek
    },
    getTopDeptors(state) {
        return state.topDeptors
    },
    getTopPoliceOfficers(state) {
        return state.topPoliceOfficers
    },
    getJobApplicationsCounts(state) {
        return state.jobApplciationCounts
    },
    isLatestCriminalRecordsLoading(state) {
        return state.loading.latestCriminalRecords
    },
    isTopCriminalsOfWeekLoading(state) {
        return state.loading.topCriminalsOfWeek
    },
    isTopdeptorsLoading(state) {
        return state.loading.topDeptors
    },
    isTopPoliceOfficersLoading(state) {
        return state.loading.topPoliceOfficers
    },
    isJobApplicationCountsLoading(state) {
        return state.loading.jobApplciationCounts
    }
}
const mutations = {
    setLatestCriminalRecords(state, criminalRecords) {
        state.latestCriminalRecords = criminalRecords
    },
    setTopCriminalsOfWeek(state, top) {
        state.topCriminalsOfWeek = top
    },
    setTopDeptors(state, top) {
        state.topDeptors = top
    },
    setTopPoliceOfficers(state, top) {
        state.topPoliceOfficers = top
    },
    setJobApplicationCounts(state, counts) {
        state.jobApplciationCounts.total = counts.total
        state.jobApplciationCounts.unchecked = counts.unchecked
        state.jobApplciationCounts.waiting = counts.waiting
    },
    toggleLatestCrimnialRecordsLoading(state, isLoading) {
        state.loading.latestCriminalRecords = isLoading
    },
    toggleTopCriminalsOfWeekLoading(state, isLoading) {
        state.loading.topCriminalsOfWeek = isLoading
    },
    toggleTopDeptorsLoading(state, isLoading) {
        state.loading.topDeptors = isLoading
    },
    toggleTopPoliceOfficersLoading(state, isLoading) {
        state.loading.topPoliceOfficers = isLoading
    },
    toggleJobApplicationsCountsLoading(state, isLoading) {
        state.loading.jobApplciationCounts = isLoading
    }
}
const actions = {
    latestCriminalRecordsAction({ commit }, token) {
        commit('toggleLatestCrimnialRecordsLoading', true)
        axios.get(
            'police/getLatestCiminalRecords',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setLatestCriminalRecords', response.data.records)
            commit('toggleLatestCrimnialRecordsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleLatestCrimnialRecordsLoading', false)
        })
    },
    topCriminelasOfWeekAction({ commit }, token) {
        commit('toggleTopCriminalsOfWeekLoading', true)
        axios.get(
            'police/getTopCriminalsOfWeek',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopCriminalsOfWeek', response.data.records)
            commit('toggleTopCriminalsOfWeekLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopCriminalsOfWeekLoading', false)
        })
    },
    topDeptorsAction({ commit }, token) {
        commit('toggleTopDeptorsLoading', true)
        axios.get(
            'police/getTopDebtors',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopDeptors', response.data.deptors)
            commit('toggleTopDeptorsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopDeptorsLoading', false)
        })
    },
    topPoliceOfficersAction({ commit }, token) {
        commit('toggleTopPoliceOfficersLoading', true)
        axios.get(
            'police/getTopPolices',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopPoliceOfficers', response.data.officers)
            commit('toggleTopPoliceOfficersLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopPoliceOfficersLoading', false)
        })
    },
    jobApplicationCountsAction({ commit }, token) {
        commit('toggleJobApplicationsCountsLoading', true)
        axios.get(
            'police/getJobAppliactionsCounts',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setJobApplicationCounts', response.data)
            commit('toggleJobApplicationsCountsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleJobApplicationsCountsLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}