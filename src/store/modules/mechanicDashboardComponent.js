import axios from 'axios'

const state = {
    topMechanicDeptors: {},
    topMechaniceWorkers: {},
    loading: {
        topMechanicDeptors: false,
        topMechaniceWorkers: false,
    }
}
const getters = {
    
    getTopMechanicDeptors(state) {
        return state.topMechanicDeptors
    },
    getTopMechanicWorkers(state) {
        return state.topMechaniceWorkers
    },
    isTopMechanicDeptorsLoading(state) {
        return state.loading.topMechanicDeptors
    },
    isTopMechanicWorkersLoading(state) {
        return state.loading.topMechaniceWorkers
    }
}
const mutations = {
    setTopMechanicDeptors(state, top) {
        state.topMechanicDeptors = top
    },
    setTopMechanicWorkers(state, top) {
        state.topMechaniceWorkers = top
    },
    toggleTopMechanicDeptorsLoading(state, isLoading) {
        state.loading.topMechanicDeptors = isLoading
    },
    toggleTopMechanicWorkersLoading(state, isLoading) {
        state.loading.topMechaniceWorkers = isLoading
    }
}
const actions = {
    topMechanicDeptorsAction({ commit }, token) {
        commit('toggleTopMechanicDeptorsLoading', true)
        axios.get(
            'mechanic/getTopDebtors',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopMechanicDeptors', response.data.deptors)
            commit('toggleTopMechanicDeptorsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopMechanicDeptorsLoading', false)
        })
    },
    topMechanicWorkersAction({ commit }, token) {
        commit('toggleTopMechanicWorkersLoading', true)
        axios.get(
            'mechanic/getTopWorkers',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopMechanicWorkers', response.data.workers)
            commit('toggleTopMechanicWorkersLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopMechanicWorkersLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}