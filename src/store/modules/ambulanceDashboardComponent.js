import axios from 'axios'

const state = {
    topAmbulanceDeptors: {},
    topAmbulanceWorkers: {},
    ambulanceJobApplciationCounts: {
        total: 0,
        unchecked: 0,
        waiting: 0
    },
    loading: {
        topAmbulanceDeptors: false,
        topAmbulanceWorkers: false,
        ambulanceJobApplciationCounts: false
    }
}
const getters = {
    
    getTopAmbulanceDeptors(state) {
        return state.topAmbulanceDeptors
    },
    getTopAmbulanceWorkers(state) {
        return state.topAmbulanceWorkers
    },
    getAmbulanceJobApplicationsCounts(state) {
        return state.ambulanceJobApplciationCounts
    },
    isTopAmbulancedeptorsLoading(state) {
        return state.loading.topAmbulanceDeptors
    },
    isTopAmbulanceWorkersLoading(state) {
        return state.loading.topAmbulanceWorkers
    },
    isAmbulanceJobApplicationCountsLoading(state) {
        return state.loading.ambulanceJobApplciationCounts
    }
}
const mutations = {
    setTopAmbulanceDeptors(state, top) {
        state.topAmbulanceDeptors = top
    },
    setTopAmbulanceWorkers(state, top) {
        state.topAmbulanceWorkers = top
    },
    setAmbulanceJobApplicationCounts(state, counts) {
        state.ambulanceJobApplciationCounts.total = counts.total
        state.ambulanceJobApplciationCounts.unchecked = counts.unchecked
        state.ambulanceJobApplciationCounts.waiting = counts.waiting
    },
    toggleTopAmbulanceDeptorsLoading(state, isLoading) {
        state.loading.topAmbulanceDeptors = isLoading
    },
    toggleTopAmbulanceWorkersLoading(state, isLoading) {
        state.loading.topAmbulanceWorkers = isLoading
    },
    toggleAmbulanceJobApplicationsCountsLoading(state, isLoading) {
        state.loading.ambulanceJobApplciationCounts = isLoading
    }
}
const actions = {
    topAmbulanceDeptorsAction({ commit }, token) {
        commit('toggleTopAmbulanceDeptorsLoading', true)
        axios.get(
            'ambulance/getTopDebtors',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopAmbulanceDeptors', response.data.deptors)
            commit('toggleTopAmbulanceDeptorsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopAmbulanceDeptorsLoading', false)
        })
    },
    topAmbulanceWorkersAction({ commit }, token) {
        commit('toggleTopAmbulanceWorkersLoading', true)
        axios.get(
            'ambulance/getTopWorkers',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopAmbulanceWorkers', response.data.workers)
            commit('toggleTopAmbulanceWorkersLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopAmbulanceWorkersLoading', false)
        })
    },
    ambulanceJobApplicationsCountsAction({ commit }, token) {
        commit('toggleAmbulanceJobApplicationsCountsLoading', true)
        axios.get(
            'ambulance/getJobAppliactionsCounts',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setAmbulanceJobApplicationCounts', response.data)
            commit('toggleAmbulanceJobApplicationsCountsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleAmbulanceJobApplicationsCountsLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}