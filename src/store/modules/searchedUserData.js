import axios from 'axios'

const state = {
    latestSearhedLoading: {
        userData: false,
        licenses: false,
        companyMoney: false,
        carsCount: false,
        cars: false,
        properties: false,
        houses: false,
        criminalRecordCounts: false,
        medicalRecordsCount: false
    },
    latestsearhedCharacterData: {
        identifier: null,
        fullName: null,
        dateOfBirth: null,
        sex: null,
        job: null,
        height: null,
        phoneNumber: null,
        jobGrade: null,
        money: null,
        bank: null,
        blackMoney: null,
        billsSum: null,
        jail: null,
        pet: null,
        jobLabel: null,
        jobGradeLabel: null,
        salary: null
    },
    latestSearchedWebUserData: {
        id: null,
        email: null,
        name: null
    },
    latestsearchedCharacterLicenses: {
        aCategory: false,
        bCategory: false,
        boat: false,
        cCategory: false,
        dCategory: false,
        fishing: false,
        hunting: false,
        theory: false,
        weapon: false,
    },
    latestsearchedCharacterWantedRecords: null,
    companyMoney: 0,
    cars: [],
    properties: [],
    houses: [],
    latestsearchedCharacterBills: [],
    criminalRecords: {
        counts: {
            total: 0,
            criminalCases: 0,
            activeCriminalCases: 0
        },
        records: []
    },
    medicalRecords: {
        counts: {
            total: 0,
            week: 0
        },
        records: []
    },
    latestsearchedCharacterPunishments: [],
    isLatestSearchedCharaceterBanned: false,
}
const getters = {
    getLatestSearchedCharacterData(state) {
        return state.latestsearhedCharacterData
    },
    getLatestSearchedWebUserData(state) {
        return state.latestSearchedWebUserData
    },
    isLatestSeachedCharacterDataLoading(state) {
        return state.latestSearhedLoading.userData
    },
    getLatestSearchedCharacterLicenses(state) {
        return state.latestsearchedCharacterLicenses
    },
    isLatestSeachedCharacterLicensesLoading(state) {
        return state.latestSearhedLoading.licenses
    },
    getLatestSearchedCharacterCompanyMoney(state) {
        return state.companyMoney
    },
    isLatestSeachedCharacterCompanyMoneyLoading(state) {
        return state.latestSearhedLoading.companyMoney
    },
    getLatestSearchedCharacterCars(state) {
        return state.cars
    },
    isLatestSeachedCharacterCarsLoading(state) {
        return state.latestSearhedLoading.cars
    },
    getLatestSearchedCharacterProperties(state) {
        return state.properties
    },
    getLatestSearchedCharacterHouses(state) {
        return state.houses
    },
    isLatestSeachedCharacterPropertiesLoading(state) {
        return state.latestSearhedLoading.properties
    },
    isLatestSeachedCharacterHousesLoading(state) {
        return state.latestSearhedLoading.houses
    },
    getLatestSearchedCharacterCriminalRecords(state) {
        return state.criminalRecords.records
    },
    getLatestSearchedCharacterCriminalRecordsCount(state) {
        return state.criminalRecords.counts
    },
    isLatestSeachedCharacterCriminalRecordsLoading(state) {
        return state.latestSearhedLoading.criminalRecordCounts
    },

    getLatestSearchedCharacterMedicalHistory(state) {
        return state.medicalRecords.records
    },
    getLatestSearchedCharacterMedicalRecordsCount(state) {
        return state.medicalRecords.counts
    },
    isLatestSeachedCharacterMedicalHistoryLoading(state) {
        return state.latestSearhedLoading.medicalRecordsCount
    },
    getLatestSeachedCharacterWantedRecords(state) {
        return state.latestsearchedCharacterWantedRecords
    },
    getLatestSeachedCharacterPunishments(state) {
        return state.latestsearchedCharacterPunishments
    },
    isLatestSearchedCharaceterBanned(state) {
        return state.isLatestSearchedCharaceterBanned
    },
    getLatestSearchedCharaceterBills(state) {
        return state.latestsearchedCharacterBills
    }
}
const mutations = {
    setLatestSearchedCharacterData(state, characterData) {
        state.latestsearhedCharacterData.identifier = characterData.identifier
        state.latestsearhedCharacterData.steamName = characterData.name
        state.latestsearhedCharacterData.fullName = characterData.fullName
        state.latestsearhedCharacterData.dateOfBirth = characterData.dateofbirth
        state.latestsearhedCharacterData.sex = characterData.sex
        state.latestsearhedCharacterData.job = characterData.job
        state.latestsearhedCharacterData.height = characterData.height
        state.latestsearhedCharacterData.phoneNumber = characterData.phoneNumber
        state.latestsearhedCharacterData.jobGrade = characterData.jobGrade
        state.latestsearhedCharacterData.money = characterData.money
        state.latestsearhedCharacterData.bank = characterData.bank
        state.latestsearhedCharacterData.jail = characterData.jail
        state.latestsearhedCharacterData.pet = characterData.pet
        state.latestsearhedCharacterData.blackMoney = characterData.blackMoney
        state.latestsearhedCharacterData.billsSum = characterData.billsSum
    },
    toggleLatestSearhcedCharacterDataLloading(state, isLoading) {
        state.latestSearhedLoading.characterData = isLoading
    },
    setLatestSearchedWebUserData(state, webUserData) {
        state.latestSearchedWebUserData.id = webUserData.id
        state.latestSearchedWebUserData.email = webUserData.email
        state.latestSearchedWebUserData.name = webUserData.name
    },
    setLatestSearchedCharacterLicenses(state, licenses) {
        state.latestsearchedCharacterLicenses = licenses
    },
    toggleLatestSearhcedCharacterLicensesLoading(state, isLoading) {
        state.latestSearhedLoading.licenses = isLoading
    },
    setLatestSearchedCharacterCompanyMoney(state, money) {
        state.companyMoney = money
    },
    toggleLatestSearhcedCharacterCompanyMoneyLoading(state, isLoading) {
        state.latestSearhedLoading.companyMoney = isLoading
    },
    setLatestSearchedCharacterCars(state, cars) {
        state.cars = cars
    },
    toggleLatestSearhcedCharacterCarsLoading(state, isLoading) {
        state.latestSearhedLoading.cars = isLoading
    },
    setLatestSearchedCharacterProperties(state, properties) {
        state.properties = properties
    },
    toggleLatestSearhcedCharacterPropertiesLoading(state, isLoading) {
        state.latestSearhedLoading.properties = isLoading
    },
    setLatestSearchedCharacterHouses(state, houses) {
        state.houses = houses
    },
    toggleLatestSearhcedCharacterHousesLoading(state, isLoading) {
        state.latestSearhedLoading.houses = isLoading
    },
    setLatestSearchedCharacterCriminalRecords(state, records) {
        state.criminalRecords.records = records
    },
    setLatestSearchedCharacterCriminalRecordsCount(state, counts) {
        state.criminalRecords.counts = counts
    },
    toggleLatestSearhcedCharacterCriminalRecordsLoading(state, isLoading) {
        state.latestSearhedLoading.criminalRecordCounts = isLoading
    },
    setLatestSearchedCharacterMedicalHistory(state, records) {
        state.medicalRecords.records = records
    },
    setLatestSearchedCharacterMedicalRecordsCount(state, counts) {
        state.medicalRecords.counts = counts
    },
    toggleLatestSearhcedCharacterMedicalHistoryLoading(state, isLoading) {
        state.latestSearhedLoading.medicalRecordsCount = isLoading
    },
    setLatestSearchedCharacterJobLabels(state, data) {
        state.latestsearhedCharacterData.jobLabel = data.jobLabel
        state.latestsearhedCharacterData.jobGradeLabel = data.jobGradeLabel
        state.latestsearhedCharacterData.salary = data.salary
    },
    setLatestsearchedCharacterWantedRecords(state, data) {
        state.latestsearchedCharacterWantedRecords = data
    },
    setLatestsearchedCharacterPunishments(state, data) {
        state.latestsearchedCharacterPunishments = data
    },
    setLatestSearchedCharaceterBanned(state, data) {
        state.isLatestSearchedCharaceterBanned = data
    },
    setLatestSearchedCharaceterbills(state, data) {
        state.latestsearchedCharacterBills = data
    }
}
const actions = {
    latesSearchedCharacterData({ commit }, data) {
        commit('toggleLatestSearhcedCharacterDataLloading', true)
        commit('toggleLatestSearhcedCharacterLicensesLoading', true)
        commit('toggleLatestSearhcedCharacterCarsLoading', true)
        commit('toggleLatestSearhcedCharacterPropertiesLoading', true)
        commit('toggleLatestSearhcedCharacterHousesLoading', true)
        commit('toggleLatestSearhcedCharacterCriminalRecordsLoading', true)
        commit('toggleLatestSearhcedCharacterMedicalHistoryLoading', true)
        axios.get(
            'user/getUserData?identifier=' + data.hex,
            { headers: { 'Authorization': 'Bearer ' + data.token } }
        ).then(response => {
            commit('setLatestSearchedCharacterData', {
                identifier: response.data.user.identifier,
                fullName: response.data.user.firstname + ' ' + response.data.user.lastname,
                dateOfBirth: response.data.user.dateofbirth,
                sex: response.data.user.sex,
                job: response.data.user.job,
                jobGrade: response.data.user.job_grade,
                height: response.data.user.height,
                phoneNumber: response.data.user.phone_number,
                money: response.data.user.money,
                bank: response.data.user.bank,
                jail: response.data.user.jail,
                pet: response.data.user.pet,
                blackMoney: response.data.user.blackMoney,
                billsSum: response.data.billsTotal
            })
            commit('setLatestSearchedWebUserData', response.data.webUser)
            commit('setLatestSearchedCharacterLicenses', response.data.licenses)
            commit('setLatestSearchedCharacterCars', response.data.user.cars)
            commit('setLatestSearchedCharacterProperties', response.data.user.owned_properties)
            commit('setLatestSearchedCharacterHouses', response.data.user.house)
            commit('setLatestSearchedCharacterCriminalRecords', response.data.user.criminal_records)
            commit('setLatestSearchedCharacterCriminalRecordsCount', response.data.criminalRecordsCount)
            commit('setLatestSearchedCharacterMedicalHistory', response.data.user.medical_history)
            commit('setLatestSearchedCharacterMedicalRecordsCount', response.data.medicalHistoryCount)
            commit('setLatestsearchedCharacterWantedRecords', response.data.user.wanted_character)
            commit('setLatestsearchedCharacterPunishments', response.data.webUser.ban_reasons)
            commit('setLatestSearchedCharaceterBanned', response.data.isBanned)
            commit('setLatestSearchedCharaceterbills', response.data.user.bills)
            commit('toggleLatestSearhcedCharacterDataLloading', false)
            commit('toggleLatestSearhcedCharacterLicensesLoading', false)
            commit('toggleLatestSearhcedCharacterCarsLoading', false)
            commit('toggleLatestSearhcedCharacterPropertiesLoading', false)
            commit('toggleLatestSearhcedCharacterCriminalRecordsLoading', false)
            commit('toggleLatestSearhcedCharacterMedicalHistoryLoading', false)

            //After user data is loaded, load job info
            axios.get(
                'user/getJobLabel',
                {
                    params: {
                        job: state.latestsearhedCharacterData.job,
                        jobGrade: state.latestsearhedCharacterData.jobGrade
                    },
                    headers: { 'Authorization': 'Bearer ' + data.token }
                }
            ).then(response => {
                commit('setLatestSearchedCharacterJobLabels', response.data)
            }).catch(error => {
                console.log(error)
            })
        }).catch(error => {
            console.log(error)
            commit('toggleLatestSearhcedCharacterDataLloading', false)
            commit('toggleLatestSearhcedCharacterLicensesLoading', false)
            commit('toggleLatestSearhcedCharacterCarsLoading', false)
            commit('toggleLatestSearhcedCharacterPropertiesLoading', false)
            commit('toggleLatestSearhcedCharacterHousesLoading', false)
            commit('toggleLatestSearhcedCharacterCriminalRecordsLoading', false)
            commit('toggleLatestSearhcedCharacterMedicalHistoryLoading', false)
        })
    },
    companyMoneyAction({ commit }, data) {
        commit('toggleLatestSearhcedCharacterCompanyMoneyLoading', true)
        axios.get(
            'user/getCompanyMoney?hex=' + data.hex,
            { headers: { 'Authorization': 'Bearer ' + data.token } }
        ).then(response => {
            commit('setLatestSearchedCharacterCompanyMoney', response.data.companyMoney)
            commit('toggleLatestSearhcedCharacterCompanyMoneyLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleLatestSearhcedCharacterCompanyMoneyLoading', false)
            commit('setLatestSearchedCharacterCompanyMoney', 0)
        })
    },
    jobDataAction({ commit }, data) {
        axios.get(
            'user/getJobLabel',
            {
                params: {
                    job: data.job,
                    jobGrade: data.jobGrade
                },
                headers: { 'Authorization': 'Bearer ' + data.token }
            }
        ).then(response => {
            commit('setLatestSearchedCharacterJobLabels', response.data)
        }).catch(error => {
            console.log(error)
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions
}