import axios from 'axios'

const state = {
    topCarModels: {},
    sellRequestCounts: {
        total: 0,
        waiting: 0
    },
    loading: {
        topCarModels: false,
        sellRequestCounts: false,
    }
}
const getters = {
    
    getTopCarModels(state) {
        return state.topCarModels
    },
    getSellRequestCounts(state) {
        return state.sellRequestCounts
    },
    isTopCarModelsLoading(state) {
        return state.loading.topCarModels
    },
    isSellRequestCountsLoading(state) {
        return state.loading.sellRequestCounts
    },
}
const mutations = {
    setTopCarModels(state, top) {
        state.topCarModels = top
    },
    setSellRequestCounts(state, counts) {
        state.sellRequestCounts.total = counts.total
        state.sellRequestCounts.waiting = counts.waiting
    },
    toggleTopCarModelsLoading(state, isLoading) {
        state.loading.topCarModels = isLoading
    },
    toggleSellRequestCountsLoading(state, isLoading) {
        state.loading.sellRequestCounts = isLoading
    },
}
const actions = {
    topCarModelsAction({ commit }, token) {
        commit('toggleTopCarModelsLoading', true)
        axios.get(
            'ark/getTopCarModels',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setTopCarModels', response.data.models)
            commit('toggleTopCarModelsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleTopCarModelsLoading', false)
        })
    },
    sellRequestCountsAction({ commit }, token) {
        commit('toggleSellRequestCountsLoading', true)
        axios.get(
            'ark/getSellRequestCounts',
            {headers: {'Authorization': 'Bearer ' + token}}
        ).then(response => {
            commit('setSellRequestCounts', response.data)
            commit('toggleSellRequestCountsLoading', false)
        }).catch(error => {
            console.log(error)
            commit('toggleSellRequestCountsLoading', false)
        })
    },
}
export default {
    state,
    getters,
    mutations,
    actions
}