import Vue from 'vue'
import App from './App.vue'
import { routes } from './routes'
import router from './routes'
import { store } from './store/store'
import axios from 'axios'
import config from './../config/env'

axios.defaults.baseURL = config.ROOT_API
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
